
package se.nackademin.librarytest.helpers;

import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.Selenide.sleep;
import se.nackademin.librarytest.model.Author;
import se.nackademin.librarytest.model.Book;
import se.nackademin.librarytest.pages.AddAuthorPage;
import se.nackademin.librarytest.pages.BookPage;
import se.nackademin.librarytest.pages.BrowseBooksPage;
import se.nackademin.librarytest.pages.MenuPage;

public class AuthorHelper {
    
    public static void addNewAuthor(Author author){
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToAddAuthor();
        sleep(1000);
        
        AddAuthorPage addAuthorPage = page(AddAuthorPage.class);
        addAuthorPage.setAuthorFirstName(author.getFirstName());
        addAuthorPage.setAuthorLastName(author.getLastName());
        addAuthorPage.setAuthorCountry(author.getCountry());
        addAuthorPage.setBiography(author.getBiography());
        
        addAuthorPage.clickAddNewAuthorButton();
        
        
    }
    
    
    
}
