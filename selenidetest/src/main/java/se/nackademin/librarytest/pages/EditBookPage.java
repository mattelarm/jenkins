
package se.nackademin.librarytest.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

public class EditBookPage extends PageBase{
    @FindBy(css = "#gwt-uid-7")
    SelenideElement editDate;
    @FindBy(css = "#save-book-button")
    SelenideElement saveBookButton;
    
    public void setDate(String date) {
       setTextFieldValue("edit date", date, editDate);
    }
    
    public void clickSaveBookButton(){
         clickButton("save book button", saveBookButton);
    }
}
