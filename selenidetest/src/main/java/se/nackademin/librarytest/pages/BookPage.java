/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author testautomatisering
 */
public class BookPage extends MenuPage{

    @FindBy(css = "#gwt-uid-3")
    SelenideElement titleField;
    @FindBy(css = "#gwt-uid-5")
    SelenideElement authorField;
    @FindBy(css = "#gwt-uid-7")
    SelenideElement descriptionField;
    @FindBy(css = "#edit-book-button")
    SelenideElement editBookButton;
    @FindBy(css = "#gwt-uid-11")
    SelenideElement dateField;
    @FindBy(css = "#borrow-book-button")
    SelenideElement borrowBookButton;
    @FindBy(css = "#gwt-uid-13")
    SelenideElement nrOfAvailableCopysField;
    @FindBy(css = "#gwt-uid-15")
    SelenideElement nrOfTotalCopys;
    @FindBy(css = "#confirmdialog-ok-button")
    SelenideElement confirmDialogYesButton;
    @FindBy(css = "#return-book-button")
    SelenideElement returnBookButton;

    public String getTitle() {
        return titleField.getText();
    }
    
    public void clickReturnBookButton(){
        clickButton("return book button", returnBookButton);
    }
    
    public void clickConfirmDialogYesButton(){
        clickButton("confirm yes button", confirmDialogYesButton);
    }
    
    public String getNrOfAvailableCopys(){
        return nrOfAvailableCopysField.getText();
    }
    
    public String getNrOfTotalCopys(){
        return nrOfTotalCopys.getText();
    }

    public String getAuthor() {
        return authorField.getText();
    }

    public String getDescription() {
        return descriptionField.getText();
    }
    
    public void clickEditBookButton(){
        clickButton("edit book button", editBookButton);
    }
    
    public String getDate(){
        return dateField.getText();
    }
    
    public void clickBorrowBookButton(){
        clickButton("borrow book button",borrowBookButton);
    }
    
}
