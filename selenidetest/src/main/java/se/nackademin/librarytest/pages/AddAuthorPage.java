
package se.nackademin.librarytest.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

public class AddAuthorPage extends MenuPage {
    @FindBy(css = "#gwt-uid-7")
    SelenideElement authorFirstName;
    @FindBy(css = "#gwt-uid-9")
    SelenideElement authorLastName;
    @FindBy(css = "#gwt-uid-3")
    SelenideElement authorCountry;
    @FindBy(css = "#gwt-uid-5")
    SelenideElement authorBiography;
    @FindBy(css = "#add-author-button")
    SelenideElement addNewAuthorButton;
    
    public void setAuthorFirstName(String firstName) {
        setTextFieldValue("first name field", firstName, authorFirstName);
    }
    
    public void setAuthorLastName(String lastName) {
        setTextFieldValue("last name field", lastName, authorLastName);
    }

    public void setAuthorCountry(String country) {
        setTextFieldValue("country field", country, authorCountry);
    }
    
    public void setBiography(String biography) {
        setTextFieldValue("password field", biography, authorBiography);
    }
    
    public void clickAddNewAuthorButton() {
        clickButton("add author button", addNewAuthorButton);
    }
}
