/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * @author mattias
 */
public class EditUserPage extends PageBase{
    @FindBy(css = "#gwt-uid-13")
    SelenideElement editEmail;
    @FindBy(css = "#save-user-button")
    SelenideElement saveUserButton;
    
    public void setEmail(String email) {
        setTextFieldValue("email field", email, editEmail);
    }
    
    public void clickSaveUser(){
        clickButton("save user button", saveUserButton);
    }
}
