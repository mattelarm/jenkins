
package se.nackademin.librarytest.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

public class BrowseAuthorPage extends PageBase{
    @FindBy(css = "#gwt-uid-3")
    private SelenideElement nameField;
    @FindBy(css = "#gwt-uid-5")
    private SelenideElement countryField;
    @FindBy(css = "#search-authors-button")
    private SelenideElement searchAuthorButton;
    
    @FindBy(css = "td.v-grid-cell:nth-child(1) > a:nth-child(1)")
    private SelenideElement firstResultTitle;

    public void clickFirstResultTitle() {
        firstResultTitle.click();
    }

    public void setNameField(String name) {
        setTextFieldValue("name field", name, nameField);
    }
    
    public void setCountryField(String country) {
        setTextFieldValue("country field", country, countryField);
    }

    public void clickSearchAuthorButton() {
        clickButton("search author button", searchAuthorButton);
    }
}
