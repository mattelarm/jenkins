package se.nackademin.librarytest;

import static com.codeborne.selenide.Selenide.*;

import java.util.UUID;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Ignore;
import se.nackademin.librarytest.helpers.AuthorHelper;
import se.nackademin.librarytest.helpers.BookHelper;
import se.nackademin.librarytest.helpers.Table;
import se.nackademin.librarytest.helpers.UserHelper;
import se.nackademin.librarytest.model.Author;
import se.nackademin.librarytest.model.Book;
import se.nackademin.librarytest.pages.AuthorPage;
import se.nackademin.librarytest.pages.BookPage;
import se.nackademin.librarytest.pages.BrowseAuthorPage;
import se.nackademin.librarytest.pages.BrowseBooksPage;
import se.nackademin.librarytest.pages.EditBookPage;
import se.nackademin.librarytest.pages.EditUserPage;
import se.nackademin.librarytest.pages.MenuPage;
import se.nackademin.librarytest.pages.MyProfilePage;

public class SelenideTest extends TestBase {

    public SelenideTest() {
    }

    @Test
    public void testChangeEmail(){
        String uuid = UUID.randomUUID().toString();
        
        MenuPage menuPage = page(MenuPage.class);
        
        UserHelper.createNewUser(uuid, uuid, uuid);
        UserHelper.logInAsUser(uuid, uuid);
        sleep(2000);
        
        menuPage.navigateToMyProfile();
        MyProfilePage myProfilePage = page(MyProfilePage.class);
        myProfilePage.clickEditUserButton();
        sleep(2000);
        
        EditUserPage editUserPage = page(EditUserPage.class);
        editUserPage.setEmail("Larm@gmail.com");
        editUserPage.clickSaveUser();
        sleep(2000);
        
        menuPage.navigateToMyProfile();
        assertEquals("email should be changed", "Larm@gmail.com", myProfilePage.getUserEmail());   
    
        UserHelper.signOutUser();
    }

    @Test
    public void testAddNewAuthor(){
        String adminName = "admin";
        String adminPassword = "1234567890";
        
        Author author = new Author();
        author.setFirstName("James");
        author.setLastName("Patterson");
        author.setCountry("USA");
        author.setBiography("James Patterson received the Literarian Award for Outstanding Service to the American Literary Community at the 2015 National Book Awards. ");
        
        MenuPage menuPage = page(MenuPage.class);
        
        UserHelper.logInAsUser(adminName, adminPassword);
        sleep(2000);
        
        AuthorHelper.addNewAuthor(author);
        sleep(2000);
        
        menuPage.navigateToBrowseAuthor();
        sleep(2000);
        BrowseAuthorPage browseAuthorPage = page(BrowseAuthorPage.class);
        browseAuthorPage.clickSearchAuthorButton();
        sleep(2000);
        Table table = new Table($(".v-grid-tablewrapper"));
        System.out.println(table.getColumnCount());
        System.out.println(table.getRowCount());
        System.out.println(table.getCellValue(0, 0));
        System.out.println(table.getCellValue(1, 1));
        table.searchAndClick("James Patterson", 0);
        sleep(2000);
        
        AuthorPage authorPage = page(AuthorPage.class);
        assertEquals("author name should be same as created", "James Patterson", authorPage.getName());
        UserHelper.signOutUser();
    }
        
    @Test
    public void changeDateOfBook() {
        String adminName = "admin";
        String adminPassword = "1234567890";
        String date = "2012-12-12";
        
        MenuPage menuPage = page(MenuPage.class);
        UserHelper.logInAsUser(adminName, adminPassword);
        sleep(2000);
        
        menuPage.navigateToBrowseBooks();
        BrowseBooksPage browseBooksPage = page(BrowseBooksPage.class);
        browseBooksPage.clickSearchBooksButton();
        Table table = new Table($(".v-grid-tablewrapper"));
        table.searchAndClick("Starship Troopers", 0);
        sleep(2000);
        
        BookPage bookPage = page(BookPage.class);
        bookPage.clickEditBookButton();
        EditBookPage editBookPage = page(EditBookPage.class);
        editBookPage.setDate(date);
        editBookPage.clickSaveBookButton();
        sleep(2000);
        
        menuPage.navigateToBrowseBooks();
        browseBooksPage.clickSearchBooksButton();
        table.searchAndClick("Starship Troopers", 0);
        sleep(1000);
        
        assertEquals("date should be the same", date, bookPage.getDate());
        UserHelper.signOutUser();
    }
    
    @Test
    public void testBorrowBook(){
        String uuid = UUID.randomUUID().toString();
        
        MenuPage menuPage = page(MenuPage.class);
        UserHelper.createNewUser(uuid, uuid);
        UserHelper.logInAsUser(uuid, uuid);
        sleep(2000);

        menuPage.navigateToBrowseBooks();
        BrowseBooksPage browseBooksPage = page(BrowseBooksPage.class);
        browseBooksPage.clickSearchBooksButton();
        sleep(2000);
        Table table = new Table($(".v-grid-tablewrapper"));
        table.searchAndClick("Starship Troopers", 0);
        sleep(2000);
        
        BookPage bookPage = page(BookPage.class);
        int nrOfAvailableCopys = Integer.parseInt(bookPage.getNrOfAvailableCopys());
        sleep(2000);
        bookPage.clickBorrowBookButton();
        bookPage.clickConfirmDialogYesButton();
        sleep(2000);
        
        assertTrue("should be true if borrow worked", nrOfAvailableCopys > Integer.parseInt(bookPage.getNrOfAvailableCopys()));
        sleep(2000);
        
        menuPage.navigateToMyProfile();
        table.searchAndClick("Starship Troopers", 0);
        sleep(1000);
        bookPage.clickReturnBookButton();
        bookPage.clickConfirmDialogYesButton();
        
        UserHelper.signOutUser();
        
    }
    
    
    
    
    
    
}
